﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SQLite;

namespace Notifyme.Models
{
    public class NotificationTable
    {
        [PrimaryKey, AutoIncrement]
        public int NotificationId { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationDescription { get; set; }
        public DateTime NotificationDateTime { get; set; }
    }

    public class DisplayItemTable : INotifyPropertyChanged
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string ImageUrl { get; set; }

        [Ignore]
        string _LikeImage { get; set; }
        [Ignore]
        public string LikeImage
        {
            get { return _LikeImage; }
            set { _LikeImage = value; OnPropertyChanged();  }
        }

        [Ignore]
        string _DisLikeImage { get; set; }
        [Ignore]
        public string DisLikeImage
        {
            get { return _DisLikeImage; }
            set { _DisLikeImage = value; OnPropertyChanged(); }
        }

        private ItemState _state;

        [Ignore]
        public ItemState state {
            get { return _state; }
            set {
                _state = value;
                OnPropertyChanged();
                ManageImage(); }
        }

        
        private void ManageImage()
        {
            LikeImage = state == ItemState.Liked ? "likeBlue.png" : "like.png";
            DisLikeImage = state == ItemState.DisLiked ? "dislikeRed.png" : "dislike.png";
            OnPropertyChanged();
        }

        public DisplayItemTable()
        {
            ManageImage();
        }

        #region PropertyChange

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetProperty<T>(ref T property, T value, [CallerMemberName] string propertyName = null)
        {
            if (!Object.Equals(property, value))
            {
                property = value;
                OnPropertyChanged(propertyName);
                return true;
            }

            return false;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
