﻿using System;

using Xamarin.Forms;

namespace Notifyme.Models
{
    public class Monkey 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string ImageUrl { get; set; }
        public ItemState state { get; set; } = ItemState.NoAction;

        public override string ToString()
        {
            return Name;
        }
    }

    public enum ItemState
    {
        NoAction=0,
        Liked=1,
        DisLiked=2
    }
}

