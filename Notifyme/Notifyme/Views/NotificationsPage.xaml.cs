﻿using System;
using System.Collections.Generic;
using Notifyme.ViewModels;
using Xamarin.Forms;

namespace Notifyme.Views
{
    public partial class NotificationsPage : ContentPage
    {
        public NotificationsPage()
        {
            InitializeComponent();

            if (App._notificationPageViewModel != null)
                BindingContext = App._notificationPageViewModel;
            else
               BindingContext = App._notificationPageViewModel = new NotificationViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((NotificationViewModel)this.BindingContext).BindData(false);
        }
    }
}
