﻿using System;
using System.Collections.Generic;
using Notifyme.Models;
using Notifyme.ViewModels;
using Xamarin.Forms;

namespace Notifyme.Views
{
    public partial class TempPage : ContentPage
    {
        public IList<Monkey> Monkeys { get; private set; }

        public TempPage()
        {
            InitializeComponent();


            BindingContext = new TempPageViewModel(); 
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((TempPageViewModel)this.BindingContext).BindData();
        }

        async void Like_Clicked(System.Object sender, System.EventArgs e)
        {
            //each push notification should contain username and information about the user device and some text
            await DisplayAlert("Liked!!!", "Send push notification", "OK");
        }

        async  void DisLike_Clicked(System.Object sender, System.EventArgs e)
        {
            //each push notification should contain username and information about the user device and some text

            await DisplayAlert("No like!!!", "Send push notification", "OK");
        }
    }
}
