﻿using System;
using Notifyme.Helper;
using Notifyme.ViewModels;
using Notifyme.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notifyme
{
    public partial class App : Application
    {
        static LocalDataBase database;
        public static NotificationViewModel _notificationPageViewModel;
        public App()
        {
            InitializeComponent();

          
            MainPage = new NavigationPage(new LoginPage());

            var temp = Database;
        }

        public static LocalDataBase Database
        {
            get
            {
                if (database == null)
                {
                    database = new LocalDataBase();
                }
                return database;
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
