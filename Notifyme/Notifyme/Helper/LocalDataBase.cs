﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Notifyme.Models;
using SQLite;

namespace Notifyme.Helper
{
    public class LocalDataBase
    {
        static readonly Lazy<SQLiteAsyncConnection> lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteAsyncConnection Database => lazyInitializer.Value;
        static bool initialized = false;

        public LocalDataBase()
        {
            InitializeAsync().SafeFireAndForget(false);
        }

        async Task InitializeAsync()
        {
            if (!initialized)
            {
                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(NotificationTable).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(NotificationTable)).ConfigureAwait(false);
                    initialized = true;
                }

                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(DisplayItemTable).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(DisplayItemTable)).ConfigureAwait(false);
                    initialized = true;
                }
            }
        }

        public Task<List<NotificationTable>> GetAllNotificationAsync()
        {
            return Database.Table<NotificationTable>().ToListAsync();
        }

        public Task<int> SaveNotificationAsync(NotificationTable item)
        {
            if (item.NotificationId != 0)
            {
                return Database.UpdateAsync(item);
            }
            else
            {
                return Database.InsertAsync(item);
            }
        }

        public Task<int> DeleteNotificationAsync(NotificationTable item)
        {
            return Database.DeleteAsync(item);
        }

        public Task<List<DisplayItemTable>> GetAllDisplayItemsAsync()
        {
            return Database.Table<DisplayItemTable>().ToListAsync();
        }

        public Task<DisplayItemTable> GetDisplayItemAsync(int id)
        {
            return Database.Table<DisplayItemTable>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveDisplayItemAsync(DisplayItemTable item)
        {
            if (item.Id != 0)
            {
                return Database.UpdateAsync(item);
            }
            else
            {
                return Database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(DisplayItemTable item)
        {
            return Database.DeleteAsync(item);
        }
    }
}
