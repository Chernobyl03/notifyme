﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Notifyme.Helper;
using Notifyme.Models;
using Xamarin.Forms;

namespace Notifyme.ViewModels
{
    public class TempPageViewModel : INotifyPropertyChanged
    {
        ObservableCollection<DisplayItemTable> _listData = new ObservableCollection<DisplayItemTable>();
        public ObservableCollection<DisplayItemTable> ListData
        {
            get { return _listData; }
            set { _listData = value; OnPropertyChanged(); }
        }


        public ICommand LikeCommand
        {
            get
            {
                return new Command<DisplayItemTable>(async (arg) =>
                {
                    try
                    {
                        if (arg != null)
                        {
                            /// Save details to local database (save notification to localDatabase)
                            arg.state = ItemState.Liked;
                            await App.Database.SaveDisplayItemAsync(arg);
                            await App.Database.SaveNotificationAsync(new NotificationTable()
                            {
                                NotificationDateTime = DateTime.Now.Date,
                                NotificationTitle = "Yay!, You liked post.",
                                NotificationDescription = $"Image Name : {arg.Name}",
                            });

                            App._notificationPageViewModel.BindData();

                            //// Mock sendNotification service replace this with rest api call
                            await NotificationDispatcher.SendTemplateNotificationsAsync($"Yay!, You liked post. Image Name : {arg.Name}");
                            /// call your rest api at this line
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex);
                    }
                    
                });
            }
        }

        public ICommand DisLikeCommand
        {
            get
            {
                return new Command<DisplayItemTable>(async (arg) =>
                {
                    try
                    {
                        if (arg != null)
                        {
                            /// Save details to local database (save notification to localDatabase)
                            arg.state = ItemState.DisLiked;
                            await App.Database.SaveDisplayItemAsync(arg);
                            await App.Database.SaveNotificationAsync(new NotificationTable()
                            {
                                NotificationDateTime = DateTime.Now.Date,
                                NotificationTitle = "Opps!, You Disliked post.",
                                NotificationDescription = $"Image Name : {arg.Name}",
                            });
                            App._notificationPageViewModel.BindData();
                            //// Mock sendNotification service replace this with rest api call
                            await NotificationDispatcher.SendTemplateNotificationsAsync($"Opps!, You Disliked post. Image Name : {arg.Name}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex);
                    }
                    
                });
            }
        }

        public TempPageViewModel()
        {

        }

        public async void BindData()
        {
            try
            {
                var DisplayData = await App.Database.GetAllDisplayItemsAsync();
                if (DisplayData != null && DisplayData.Count > 0)
                {
                    ListData = new ObservableCollection<DisplayItemTable>(DisplayData);
                }
                else
                {
                    await TempBindData();
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        private async Task TempBindData()
        {
            var tempListData = new ObservableCollection<DisplayItemTable>();
            tempListData.Add(new DisplayItemTable
            {
                Name = "Baboon",
                Location = "Africa & Asia",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Papio_anubis_%28Serengeti%2C_2009%29.jpg/200px-Papio_anubis_%28Serengeti%2C_2009%29.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Capuchin Monkey",
                Location = "Central & South America",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Capuchin_Costa_Rica.jpg/200px-Capuchin_Costa_Rica.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Blue Monkey",
                Location = "Central and East Africa",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/BlueMonkey.jpg/220px-BlueMonkey.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Squirrel Monkey",
                Location = "Central & South America",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Saimiri_sciureus-1_Luc_Viatour.jpg/220px-Saimiri_sciureus-1_Luc_Viatour.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Golden Lion Tamarin",
                Location = "Brazil",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Golden_lion_tamarin_portrait3.jpg/220px-Golden_lion_tamarin_portrait3.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Howler Monkey",
                Location = "South America",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Alouatta_guariba.jpg/200px-Alouatta_guariba.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Japanese Macaque",
                Location = "Japan",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Macaca_fuscata_fuscata1.jpg/220px-Macaca_fuscata_fuscata1.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Mandrill",
                Location = "Southern Cameroon, Gabon, Equatorial Guinea, and Congo",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Mandrill_at_san_francisco_zoo.jpg/220px-Mandrill_at_san_francisco_zoo.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Proboscis Monkey",
                Location = "Borneo",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Proboscis_Monkey_in_Borneo.jpg/250px-Proboscis_Monkey_in_Borneo.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Red-shanked Douc",
                Location = "Vietnam, Laos",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Portrait_of_a_Douc.jpg/159px-Portrait_of_a_Douc.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Gray-shanked Douc",
                Location = "Vietnam",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cuc.Phuong.Primate.Rehab.center.jpg/320px-Cuc.Phuong.Primate.Rehab.center.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Golden Snub-nosed Monkey",
                Location = "China",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Golden_Snub-nosed_Monkeys%2C_Qinling_Mountains_-_China.jpg/165px-Golden_Snub-nosed_Monkeys%2C_Qinling_Mountains_-_China.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Black Snub-nosed Monkey",
                Location = "China",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/RhinopitecusBieti.jpg/320px-RhinopitecusBieti.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Tonkin Snub-nosed Monkey",
                Location = "Vietnam",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Tonkin_snub-nosed_monkeys_%28Rhinopithecus_avunculus%29.jpg/320px-Tonkin_snub-nosed_monkeys_%28Rhinopithecus_avunculus%29.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Thomas's Langur",
                Location = "Indonesia",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Thomas%27s_langur_Presbytis_thomasi.jpg/142px-Thomas%27s_langur_Presbytis_thomasi.jpg"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Purple-faced Langur",
                Location = "Sri Lanka",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Semnopithèque_blanchâtre_mâle.JPG/192px-Semnopithèque_blanchâtre_mâle.JPG"
            });

            tempListData.Add(new DisplayItemTable
            {
                Name = "Gelada",
                Location = "Ethiopia",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Gelada-Pavian.jpg/320px-Gelada-Pavian.jpg"
            });


            foreach (var item in tempListData)
            {
                await App.Database.SaveDisplayItemAsync(item);
            }
        }

        #region PropertyChange
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetProperty<T>(ref T property, T value, [CallerMemberName] string propertyName = null)
        {
            if (!Object.Equals(property, value))
            {
                property = value;
                OnPropertyChanged(propertyName);
                return true;
            }

            return false;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
