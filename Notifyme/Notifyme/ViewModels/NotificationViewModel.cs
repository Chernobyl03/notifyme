﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Notifyme.Models;
using Xamarin.Forms;

namespace Notifyme.ViewModels
{
    public class NotificationViewModel : INotifyPropertyChanged
    {
        ObservableCollection<NotificationTable> _listData = new ObservableCollection<NotificationTable>();
        public ObservableCollection<NotificationTable> ListData
        {
            get { return _listData; }
            set { _listData = value; OnPropertyChanged(); }
        }


        string _count;
        public string Count
        {
            get { return _count; }
            set { _count = value; OnPropertyChanged(); }
        }

        public ICommand DeleteCommand
        {
            get
            {
                return new Command<NotificationTable>(async (arg) =>
                {
                    try
                    {
                        if (arg != null)
                        {
                            /// delete notification code here, can replace with api call 
                            await App.Database.DeleteNotificationAsync(arg);
                            BindData(false);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex);
                    }

                });
            }
        }

        public NotificationViewModel()
        {
        }

        public async void BindData(bool showBadge = true)
        {
            try
            {
                /// Retrive notifications from Local DB
                var DisplayData = await App.Database.GetAllNotificationAsync();
                if (DisplayData != null && DisplayData.Count > 0)
                {
                    ListData = new ObservableCollection<NotificationTable>(DisplayData);
                }
                else
                {
                    ListData = new ObservableCollection<NotificationTable>();
                }

                if (!showBadge)
                {
                    RemoveBadege();
                    return;
                }

                switch (ListData.Count)
                {
                    case 0:
                        Count = null;
                        break;
                    case 1:
                        Count = "1";
                        break;
                    case 2:
                        Count = "2";
                        break;
                    default:
                        Count = "2+";
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        public void RemoveBadege()
        {
            Count = null;
        }


        #region PropertyChange
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetProperty<T>(ref T property, T value, [CallerMemberName] string propertyName = null)
        {
            if (!Object.Equals(property, value))
            {
                property = value;
                OnPropertyChanged(propertyName);
                return true;
            }

            return false;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
