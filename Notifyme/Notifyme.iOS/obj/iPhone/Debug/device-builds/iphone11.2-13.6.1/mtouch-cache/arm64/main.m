#include "xamarin/xamarin.h"

extern void *mono_aot_module_Notifyme_iOS_info;
extern void *mono_aot_module_mscorlib_info;
extern void *mono_aot_module_Xamarin_Forms_Core_info;
extern void *mono_aot_module_netstandard_info;
extern void *mono_aot_module_System_Core_info;
extern void *mono_aot_module_System_info;
extern void *mono_aot_module_Mono_Security_info;
extern void *mono_aot_module_System_Xml_info;
extern void *mono_aot_module_System_Numerics_info;
extern void *mono_aot_module_System_Data_info;
extern void *mono_aot_module_System_Transactions_info;
extern void *mono_aot_module_System_Data_DataSetExtensions_info;
extern void *mono_aot_module_System_Drawing_Common_info;
extern void *mono_aot_module_System_IO_Compression_info;
extern void *mono_aot_module_System_IO_Compression_FileSystem_info;
extern void *mono_aot_module_System_ComponentModel_Composition_info;
extern void *mono_aot_module_System_Net_Http_info;
extern void *mono_aot_module_Xamarin_iOS_info;
extern void *mono_aot_module_System_Runtime_Serialization_info;
extern void *mono_aot_module_System_ServiceModel_Internals_info;
extern void *mono_aot_module_System_Web_Services_info;
extern void *mono_aot_module_System_Xml_Linq_info;
extern void *mono_aot_module_Xamarin_Forms_Platform_info;
extern void *mono_aot_module_Xamarin_Forms_Platform_iOS_info;
extern void *mono_aot_module_Plugin_Badge_iOS_info;
extern void *mono_aot_module_Plugin_Badge_Abstractions_info;
extern void *mono_aot_module_Xamarin_Azure_NotificationHubs_iOS_info;
extern void *mono_aot_module_Notifyme_info;
extern void *mono_aot_module_Xamarin_Forms_Xaml_info;
extern void *mono_aot_module_SQLite_net_info;
extern void *mono_aot_module_SQLitePCLRaw_core_info;
extern void *mono_aot_module_System_Memory_info;
extern void *mono_aot_module_SQLitePCLRaw_batteries_v2_info;
extern void *mono_aot_module_SQLitePCLRaw_nativelibrary_info;
extern void *mono_aot_module_SQLitePCLRaw_provider_dynamic_cdecl_info;
extern void *mono_aot_module_Microsoft_Azure_NotificationHubs_info;
extern void *mono_aot_module_Newtonsoft_Json_info;
extern void *mono_aot_module_System_Runtime_info;
extern void *mono_aot_module_System_Resources_ResourceManager_info;
extern void *mono_aot_module_System_IO_info;
extern void *mono_aot_module_System_Runtime_Numerics_info;
extern void *mono_aot_module_System_Xml_XmlDocument_info;
extern void *mono_aot_module_System_Xml_XDocument_info;
extern void *mono_aot_module_System_Collections_info;
extern void *mono_aot_module_System_Globalization_info;
extern void *mono_aot_module_System_Threading_Tasks_info;
extern void *mono_aot_module_System_Diagnostics_Debug_info;
extern void *mono_aot_module_System_Runtime_Serialization_Primitives_info;
extern void *mono_aot_module_System_Reflection_info;
extern void *mono_aot_module_System_ComponentModel_TypeConverter_info;
extern void *mono_aot_module_System_Dynamic_Runtime_info;
extern void *mono_aot_module_System_Linq_Expressions_info;
extern void *mono_aot_module_System_Linq_info;
extern void *mono_aot_module_System_Runtime_Serialization_Formatters_info;
extern void *mono_aot_module_System_ObjectModel_info;
extern void *mono_aot_module_System_Text_RegularExpressions_info;
extern void *mono_aot_module_System_Xml_ReaderWriter_info;
extern void *mono_aot_module_System_Text_Encoding_info;
extern void *mono_aot_module_System_Runtime_Extensions_info;
extern void *mono_aot_module_System_Threading_info;
extern void *mono_aot_module_System_Reflection_Extensions_info;
extern void *mono_aot_module_System_Reflection_Primitives_info;
extern void *mono_aot_module_System_Text_Encoding_Extensions_info;
extern void *mono_aot_module_Microsoft_CSharp_info;
extern void *mono_aot_module_Microsoft_Extensions_Caching_Abstractions_info;
extern void *mono_aot_module_Microsoft_Extensions_Primitives_info;
extern void *mono_aot_module_System_Runtime_CompilerServices_Unsafe_info;
extern void *mono_aot_module_Microsoft_Extensions_Caching_Memory_info;
extern void *mono_aot_module_Microsoft_Extensions_DependencyInjection_Abstractions_info;
extern void *mono_aot_module_Microsoft_Extensions_Options_info;

void xamarin_register_modules_impl ()
{
	mono_aot_register_module (mono_aot_module_Notifyme_iOS_info);
	mono_aot_register_module (mono_aot_module_mscorlib_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Core_info);
	mono_aot_register_module (mono_aot_module_netstandard_info);
	mono_aot_register_module (mono_aot_module_System_Core_info);
	mono_aot_register_module (mono_aot_module_System_info);
	mono_aot_register_module (mono_aot_module_Mono_Security_info);
	mono_aot_register_module (mono_aot_module_System_Xml_info);
	mono_aot_register_module (mono_aot_module_System_Numerics_info);
	mono_aot_register_module (mono_aot_module_System_Data_info);
	mono_aot_register_module (mono_aot_module_System_Transactions_info);
	mono_aot_register_module (mono_aot_module_System_Data_DataSetExtensions_info);
	mono_aot_register_module (mono_aot_module_System_Drawing_Common_info);
	mono_aot_register_module (mono_aot_module_System_IO_Compression_info);
	mono_aot_register_module (mono_aot_module_System_IO_Compression_FileSystem_info);
	mono_aot_register_module (mono_aot_module_System_ComponentModel_Composition_info);
	mono_aot_register_module (mono_aot_module_System_Net_Http_info);
	mono_aot_register_module (mono_aot_module_Xamarin_iOS_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Serialization_info);
	mono_aot_register_module (mono_aot_module_System_ServiceModel_Internals_info);
	mono_aot_register_module (mono_aot_module_System_Web_Services_info);
	mono_aot_register_module (mono_aot_module_System_Xml_Linq_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Platform_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Platform_iOS_info);
	mono_aot_register_module (mono_aot_module_Plugin_Badge_iOS_info);
	mono_aot_register_module (mono_aot_module_Plugin_Badge_Abstractions_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Azure_NotificationHubs_iOS_info);
	mono_aot_register_module (mono_aot_module_Notifyme_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Xaml_info);
	mono_aot_register_module (mono_aot_module_SQLite_net_info);
	mono_aot_register_module (mono_aot_module_SQLitePCLRaw_core_info);
	mono_aot_register_module (mono_aot_module_System_Memory_info);
	mono_aot_register_module (mono_aot_module_SQLitePCLRaw_batteries_v2_info);
	mono_aot_register_module (mono_aot_module_SQLitePCLRaw_nativelibrary_info);
	mono_aot_register_module (mono_aot_module_SQLitePCLRaw_provider_dynamic_cdecl_info);
	mono_aot_register_module (mono_aot_module_Microsoft_Azure_NotificationHubs_info);
	mono_aot_register_module (mono_aot_module_Newtonsoft_Json_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_info);
	mono_aot_register_module (mono_aot_module_System_Resources_ResourceManager_info);
	mono_aot_register_module (mono_aot_module_System_IO_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Numerics_info);
	mono_aot_register_module (mono_aot_module_System_Xml_XmlDocument_info);
	mono_aot_register_module (mono_aot_module_System_Xml_XDocument_info);
	mono_aot_register_module (mono_aot_module_System_Collections_info);
	mono_aot_register_module (mono_aot_module_System_Globalization_info);
	mono_aot_register_module (mono_aot_module_System_Threading_Tasks_info);
	mono_aot_register_module (mono_aot_module_System_Diagnostics_Debug_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Serialization_Primitives_info);
	mono_aot_register_module (mono_aot_module_System_Reflection_info);
	mono_aot_register_module (mono_aot_module_System_ComponentModel_TypeConverter_info);
	mono_aot_register_module (mono_aot_module_System_Dynamic_Runtime_info);
	mono_aot_register_module (mono_aot_module_System_Linq_Expressions_info);
	mono_aot_register_module (mono_aot_module_System_Linq_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Serialization_Formatters_info);
	mono_aot_register_module (mono_aot_module_System_ObjectModel_info);
	mono_aot_register_module (mono_aot_module_System_Text_RegularExpressions_info);
	mono_aot_register_module (mono_aot_module_System_Xml_ReaderWriter_info);
	mono_aot_register_module (mono_aot_module_System_Text_Encoding_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Extensions_info);
	mono_aot_register_module (mono_aot_module_System_Threading_info);
	mono_aot_register_module (mono_aot_module_System_Reflection_Extensions_info);
	mono_aot_register_module (mono_aot_module_System_Reflection_Primitives_info);
	mono_aot_register_module (mono_aot_module_System_Text_Encoding_Extensions_info);
	mono_aot_register_module (mono_aot_module_Microsoft_CSharp_info);
	mono_aot_register_module (mono_aot_module_Microsoft_Extensions_Caching_Abstractions_info);
	mono_aot_register_module (mono_aot_module_Microsoft_Extensions_Primitives_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_CompilerServices_Unsafe_info);
	mono_aot_register_module (mono_aot_module_Microsoft_Extensions_Caching_Memory_info);
	mono_aot_register_module (mono_aot_module_Microsoft_Extensions_DependencyInjection_Abstractions_info);
	mono_aot_register_module (mono_aot_module_Microsoft_Extensions_Options_info);

}

void xamarin_register_assemblies_impl ()
{
	guint32 exception_gchandle = 0;
	xamarin_open_and_register ("Xamarin.Forms.Platform.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);
	xamarin_open_and_register ("Plugin.Badge.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);
	xamarin_open_and_register ("Xamarin.Azure.NotificationHubs.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);

}

extern "C" void xamarin_create_classes();
extern "C" { void mono_ee_interp_init (const char *); }
extern "C" { void mono_icall_table_init (void); }
extern "C" { void mono_marshal_ilgen_init (void); }
extern "C" { void mono_method_builder_ilgen_init (void); }
extern "C" { void mono_sgen_mono_ilgen_init (void); }
void xamarin_setup_impl ()
{
	mono_icall_table_init ();
	mono_marshal_ilgen_init ();
	mono_method_builder_ilgen_init ();
	mono_sgen_mono_ilgen_init ();
	mono_ee_interp_init (NULL);
	mono_jit_set_aot_mode (MONO_AOT_MODE_INTERP);
	xamarin_create_classes();

	mono_dllmap_insert (NULL, "System.Native", NULL, "__Internal", NULL);
	mono_dllmap_insert (NULL, "System.Security.Cryptography.Native.Apple", NULL, "__Internal", NULL);
	mono_dllmap_insert (NULL, "System.Net.Security.Native", NULL, "__Internal", NULL);

	xamarin_gc_pump = FALSE;
	xamarin_init_mono_debug = TRUE;
	xamarin_executable_name = "Notifyme.iOS.exe";
	mono_use_llvm = FALSE;
	xamarin_log_level = 0;
	xamarin_arch_name = "arm64";
	xamarin_marshal_objectivec_exception_mode = MarshalObjectiveCExceptionModeDisable;
	xamarin_debug_mode = TRUE;
	setenv ("MONO_GC_PARAMS", "nursery-size=512k,major=marksweep-conc", 1);
	xamarin_supports_dynamic_registration = TRUE;
}

int main (int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int rv = xamarin_main (argc, argv, XamarinLaunchModeApp);
	[pool drain];
	return rv;
}
void xamarin_initialize_callbacks () __attribute__ ((constructor));
void xamarin_initialize_callbacks ()
{
	xamarin_setup = xamarin_setup_impl;
	xamarin_register_assemblies = xamarin_register_assemblies_impl;
	xamarin_register_modules = xamarin_register_modules_impl;
}
